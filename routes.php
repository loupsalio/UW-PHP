<?php

use Core\Router;

Router::connect('/', ['controller' => 'User', 'action' => 'index']);
Router::connect('/new_user', ['controller' => 'User', 'action' => 'add']);
