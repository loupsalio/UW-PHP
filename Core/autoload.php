<?php

function my_autoloader($class) {
    $class = str_replace('/', DIRECTORY_SEPARATOR, $class);
    $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    if (file_exists($class . ".php"))
        include ($class . '.php');
    else if (file_exists("core" . DIRECTORY_SEPARATOR . $class . ".php")){
        include ("src" . DIRECTORY_SEPARATOR . $class . '.php');
    }
    else if (file_exists("src" . DIRECTORY_SEPARATOR . "Controller". DIRECTORY_SEPARATOR . $class . ".php")){
        include ("src" . DIRECTORY_SEPARATOR . "Controller". DIRECTORY_SEPARATOR . $class . '.php');
    }
    else if (file_exists("src" . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . $class . ".php")){
        include ("src" . DIRECTORY_SEPARATOR . "View"  . DIRECTORY_SEPARATOR . $class . '.php');
    }
    else if (file_exists("src" . DIRECTORY_SEPARATOR . "Model" . DIRECTORY_SEPARATOR . $class . ".php")){
        include ("src" . DIRECTORY_SEPARATOR . "Model" . DIRECTORY_SEPARATOR . $class . '.php');
    }
    else if (file_exists("src" . DIRECTORY_SEPARATOR . $class . ".php")){
        include ("src" . DIRECTORY_SEPARATOR . $class . '.php');
    }
}

spl_autoload_register('my_autoloader');