<?php

namespace Core;

class Core {
    public function run() {
        try {
            $request = Router::get($_SERVER['REQUEST_URI']);

            $controller = $request["controller"]."Controller";
            $action=$request["action"];

            if(method_exists($controller, $request["action"])){
                $tmp = new $controller();
                $tmp->$action();
            }
            else
            {
                echo "Unknown class/method" ;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
}

