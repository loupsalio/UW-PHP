<?php

namespace Core;

use Exception;

Class Router
{
    private static $routes;

    public static function connect($url, $route)
    {
        self::$routes[$url] = $route;
    }

    public static function get($url)
    {
        if (array_key_exists($url, self::$routes))
         return self::$routes[$url];
        else
            throw new Exception("Route not found");
    }

}