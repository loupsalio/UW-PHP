<?php

use \Core\Controller;

Class UserController extends Controller {
    private $model;

    public function __construct()
    {
        $this->model = new UserModel();
    }

    public function index(){
        echo __CLASS__ . " [OK]" . PHP_EOL;
        $this->render("index");
    }

    public function add(){
        echo __CLASS__ . " [OK]" . PHP_EOL;
        if (!isset($_POST['mail']) || !isset($_POST["passwd"]))
        {
            $this->redirect("/");
            return;
        }
        $this->model->add($_POST['mail'], $_POST['passwd']);
        $this->redirect("/");
    }
}