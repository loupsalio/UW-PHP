<?php

use Core\Controller;

Class AppController extends Controller {
    public function index(){
        echo __CLASS__ . " [OK]" . PHP_EOL;
        $this->render("index");
    }

}